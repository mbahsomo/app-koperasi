var doeventDate = angular.module('dDate', []);
doeventDate.directive('dDate', function(){
    var linker = function(scope, el, attr, ngModelCtrl) {
        el.datepicker();
        el.datepicker().on('changeDate', function(){
            ngModelCtrl.$setViewValue(el.val());//value is datepicker selected date;
            $(".datepicker").hide();
        });
        /*el.datepicker().on('blur', function(){
            ngModelCtrl.$setViewValue(el.val());//value is datepicker selected date;
            $(".datepicker").hide();
        });*/
        var component = el.siblings('[data-toggle="datepicker"]');
        if (component.length) {
            component.on('click', function () {
                el.trigger('focus');
            });
        }
    }
    return {
        restrict: 'A',
        require: 'ngModel',
        link: linker
    };
});