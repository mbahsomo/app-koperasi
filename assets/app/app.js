function cssfile(url_css){
    return $("<link rel='stylesheet' type='text/css' href='"+url_css+"'>");
}

$("head").append(cssfile(base_url + 'assets/lib/bootstrap/css/bootstrap.min.css'));
/*$("head").append(cssfile(base_url + 'assets/lib/bootstrap/css/bootstrap-theme.min.css'));*/
/*$("head").append(cssfile(base_url + 'assets/lib/bootstrap/css/bootstrap-max.min.css'));*/
$("head").append(cssfile(base_url + 'assets/lib/bootstrap/css/united-bootstrap.min.css'));
$("head").append(cssfile(base_url + 'assets/css/bootstrap-theme.css'));
$("head").append(cssfile(base_url + 'assets/lib/font-awesome/css/font-awesome.min.css'));
$("head").append(cssfile(base_url + 'assets/lib/abn-tree/abn_tree.css'));
$("head").append(cssfile(base_url + 'assets/css/dashboard.css'));
$("head").append(cssfile(base_url + 'assets/css/t-table.css'));
$("head").append(cssfile(base_url + 'assets/css/style.css'));

$("head").append(cssfile(base_url + 'assets/lib/loading/loading-bar.min.css'));
$("head").append(cssfile(base_url + 'assets/lib/ng-grid/ng-grid.min.css'));
$("head").append(cssfile(base_url + 'assets/css/ng-grid-theme-biru.css'));
$("head").append(cssfile(base_url + 'assets/lib/toaster/toaster.css'));
$("head").append(cssfile(base_url + 'assets/lib/date-picker/date.css'));
$script(
    [
        base_url + 'assets/lib/jquery/jquery-ui.min.js',
        base_url + 'assets/lib/bootstrap/js/bootstrap.min.js',
        base_url + 'assets/lib/ng-grid/ng-grid.min.js',
        /*base_url + 'assets/lib/ng-grid/ng-grid.debug.js',*/
        base_url + 'assets/lib/ng-grid/ng-grid-flexible-height.js',
        base_url + 'assets/lib/angularjs/angular-route.min.js',
        base_url + 'assets/lib/angularjs/angular-animate.min.js',
        base_url + 'assets/lib/loading/loading-bar.min.js',
        base_url + 'assets/lib/toaster/toaster.js',
        base_url + 'assets/lib/doevent/doevent-print.js',
        base_url + 'assets/lib/doevent/doevent-tools.js',
        base_url + 'assets/lib/doevent/doevent-upload.js',
        base_url + 'assets/lib/angularjs/mask.js',
        base_url + 'assets/js/md5.js',
        base_url + 'assets/lib/auto-complite/ui-bootstrap-tpls-0.9.0.js',
        base_url + 'assets/lib/date-picker/date.js',
        base_url + 'assets/lib/date-picker/m-date.js',
        base_url + 'assets/lib/abn-tree/abn_tree_directive.js',
        base_url + 'assets/lib/doevent/doevent-input.js'
    ],
    'app'
);

var bahasa = [
    {"bahasa":"ina","isi":[]}
];

$script.ready('app', function() {

    var app = angular.module('SukodonoApp', ['ngRoute','ngGrid','chieffancypants.loadingBar','toaster','dFile','ui.mask','ui.bootstrap','dDate','dTools','angularBootstrapNavTree','deFocus']);

    app.filter('dateEntry', function myDateFormat($filter) {
        return function(text) {
            if (text !== null) {
                var tempdate = new Date(text.replace(/-/g, "/"));
                return $filter('date')(tempdate, "dd-MM-yyyy HH:mm:ss");
            } else {
                return $filter('date')(new Date(), "dd-MM-yyyy HH:mm:ss");
            }

        };
    });
    app.provider('jsDeps', function() {
        this.$get = function(dependencies) {
            return {jsdeps: function($q, $rootScope) {
                    var deferred = $q.defer();
                    $script(dependencies, function() {
                        $rootScope.$apply(function() {
                            deferred.resolve();
                        });
                    });
                    return deferred.promise;
                }};
        };
    });


    app.config(function($routeProvider, $controllerProvider, $compileProvider, $filterProvider, $provide, jsDepsProvider, $httpProvider)
    {
        app.controllerProvider = $controllerProvider;
        app.compileProvider = $compileProvider;
        app.routeProvider = $routeProvider;
        app.filterProvider = $filterProvider;
        app.provide = $provide;
        
        /*Master data*/
        $routeProvider.when('/depart', {
            templateUrl: site_url + 'assets/app/views/depart.html',
            title: 'Depart',
            resolve: jsDepsProvider.$get([site_url + 'assets/app/controllers/depart.js']),
            controller: 'DepartController'
        }).when('/anggota', {
            templateUrl: site_url + 'assets/app/views/anggota.html',
            title: 'Anggota',
            resolve: jsDepsProvider.$get([site_url + 'assets/app/controllers/anggota.js']),
            controller: 'AnggotaController'
        }).when('/jenis-simpanan', {
            templateUrl: site_url + 'assets/app/views/jenis-simpanan.html',
            title: 'jenis-simpanan',
            resolve: jsDepsProvider.$get([site_url + 'assets/app/controllers/jenis-simpanan.js']),
            controller: 'JenisSimpananController'
        }).when('/jenis-pinjaman', {
            templateUrl: site_url + 'assets/app/views/jenis-pinjaman.html',
            title: 'jenis-pinjaman',
            resolve: jsDepsProvider.$get([site_url + 'assets/app/controllers/jenis-pinjaman.js']),
            controller: 'JenisPinjamanController'
        }).when('/periode-keuangan', {
            templateUrl: site_url + 'assets/app/views/periode-keuangan.html',
            title: 'periodekeuangan',
            resolve: jsDepsProvider.$get([site_url + 'assets/app/controllers/periode-keuangan.js']),
            controller: 'PeriodeKeuanganController'
        }).when('/rekening', {
            templateUrl: site_url + 'assets/app/views/rekening.html',
            resolve: jsDepsProvider.$get([site_url + 'assets/app/controllers/rekening.js']),
            controller: 'RekeningController'
        });

        $routeProvider.when('/simpanan', {
            templateUrl: site_url + 'assets/app/views/simpanan.html',
            title: 'simpanan',
            resolve: jsDepsProvider.$get([site_url + 'assets/app/controllers/simpanan.js']),
            controller: 'SimpananController'
        }).when('/pengambilan-simpanan', {
            templateUrl: site_url + 'assets/app/views/pengambilan-simpanan.html',
            title: 'pengambilan-simpanan',
            resolve: jsDepsProvider.$get([site_url + 'assets/app/controllers/pengambilan-simpanan.js']),
            controller: 'PengambilanSimpananController'
        }).when('/pengajuan-pinjaman', {
            templateUrl: site_url + 'assets/app/views/pengajuan-pinjaman.html',
            title: 'pengajuan-pinjaman',
            resolve: jsDepsProvider.$get([site_url + 'assets/app/controllers/pengajuan-pinjaman.js']),
            controller: 'PengajuanPinjamanController'
        }).when('/pinjaman', {
            templateUrl: site_url + 'assets/app/views/pinjaman.html',
            title: 'pinjaman',
            resolve: jsDepsProvider.$get([site_url + 'assets/app/controllers/pinjaman.js']),
            controller: 'PinjamanController'
        }).when('/pembayaran', {
            templateUrl: site_url + 'assets/app/views/pembayaran.html',
            title: 'pembayaran',
            resolve: jsDepsProvider.$get([site_url + 'assets/app/controllers/pembayaran.js']),
            controller: 'PembayaranController'
        }).when('/jurnal-harian', {
            templateUrl: site_url + 'assets/app/views/jurnal-harian.html',
            title: 'jurnal-harian',
            resolve: jsDepsProvider.$get([site_url + 'assets/app/controllers/jurnal-harian.js']),
            controller: 'JurnalHarianController'
        }).when('/tutup-buku', {
            templateUrl: site_url + 'assets/app/views/tutup-buku.html',
            title: 'tutup-buku',
            resolve: jsDepsProvider.$get([site_url + 'assets/app/controllers/tutup-buku.js']),
            controller: 'TutupBukuController'
        }).when('/jurnal-penyesuaian', {
            templateUrl: site_url + 'assets/app/views/jurnal-penyesuaian.html',
            title: 'jurnal-penyesuaian',
            resolve: jsDepsProvider.$get([site_url + 'assets/app/controllers/jurnal-penyesuaian.js']),
            controller: 'JurnalPenyesuaianController'
        });

        $routeProvider.when('/lap-simpanan', {
            templateUrl: site_url + 'assets/app/views/rekeningkoran.html',
            title: 'Rekeningkoran',
            resolve: jsDepsProvider.$get([site_url + 'assets/app/controllers/rekeningkoran.js']),
            controller: 'RekeningkoranController'
        }).when('/lap-pengajuan-pinjaman', {
            templateUrl: site_url + 'assets/app/views/rekeningkoran.html',
            title: 'Rekeningkoran',
            resolve: jsDepsProvider.$get([site_url + 'assets/app/controllers/rekeningkoran.js']),
            controller: 'RekeningkoranController'
        }).when('/lap-pinjaman', {
            templateUrl: site_url + 'assets/app/views/rekeningkoran.html',
            title: 'Rekeningkoran',
            resolve: jsDepsProvider.$get([site_url + 'assets/app/controllers/rekeningkoran.js']),
            controller: 'RekeningkoranController'
        }).when('/lap-pinjaman', {
            templateUrl: site_url + 'assets/app/views/rekeningkoran.html',
            title: 'Rekeningkoran',
            resolve: jsDepsProvider.$get([site_url + 'assets/app/controllers/rekeningkoran.js']),
            controller: 'RekeningkoranController'
        }).when('/lap-angsuran', {
            templateUrl: site_url + 'assets/app/views/rekeningkoran.html',
            title: 'Rekeningkoran',
            resolve: jsDepsProvider.$get([site_url + 'assets/app/controllers/rekeningkoran.js']),
            controller: 'RekeningkoranController'
        }).when('/lap-jurnal', {
            templateUrl: site_url + 'assets/app/views/rekeningkoran.html',
            title: 'Rekeningkoran',
            resolve: jsDepsProvider.$get([site_url + 'assets/app/controllers/rekeningkoran.js']),
            controller: 'RekeningkoranController'
        }).when('/lap-lap-buku-besar', {
            templateUrl: site_url + 'assets/app/views/rekeningkoran.html',
            title: 'Rekeningkoran',
            resolve: jsDepsProvider.$get([site_url + 'assets/app/controllers/rekeningkoran.js']),
            controller: 'RekeningkoranController'
        }).when('/lap-neraca-saldo', {
            templateUrl: site_url + 'assets/app/views/rekeningkoran.html',
            title: 'Rekeningkoran',
            resolve: jsDepsProvider.$get([site_url + 'assets/app/controllers/rekeningkoran.js']),
            controller: 'RekeningkoranController'
        }).when('/lap-laba-rugi', {
            templateUrl: site_url + 'assets/app/views/rekeningkoran.html',
            title: 'Rekeningkoran',
            resolve: jsDepsProvider.$get([site_url + 'assets/app/controllers/rekeningkoran.js']),
            controller: 'RekeningkoranController'
        });
    });


    app.controller('MenuCtrl', function($scope, $timeout, $location, $http, $filter) {
        var apple_selected, tree, treedata_avm, treedata_geography;
        $scope.show_reload = false; 
        $scope.my_tree_handler = function(branch) {
            var _ref;
            //console.log(branch);
            if (branch.url !== undefined) {
                $location.path(branch.url);
            }
            $scope.output = "You selected: " + branch.label;
        };
        
        treedata_avm = [
            {
                label : 'Master',
                children : [
                    {
                        label : 'Umum / Koperasi',
                        children : [   
                            {
                                label : 'Depart',
                                url : 'depart'
                            },{
                                label : 'Anggota',
                                url : 'anggota'
                            },{
                                label : 'Jenis Simpanan',
                                url : 'jenis-simpanan'
                            },{
                                label : 'Jenis Pinjaman',
                                url : 'jenis-pinjaman'
                            }
                        ]
                    },{
                        label : 'Keuangan',
                        children : [
                            {
                                label : 'Periode Keuangan',
                                url : 'periode-keuangan'
                            },{
                                label : 'Rekening',
                                url : 'rekening'
                            }
                        ]
                    }
                ]
            },{
                label : 'Transaksi',
                children : [
                    {
                        label : 'Koperasi',
                        children : [
                            {
                                label : 'Simpanan',
                                url : 'simpanan'
                            },{
                                label : 'Pengambilan Simpanan',
                                url : 'pengambilan-simpanan'
                            },{
                                label : 'Pengajuan Pinjaman',
                                url : 'pengajuan-pinjaman'
                            },{
                                label : 'Pinjaman',
                                url : 'pinjaman'
                            },{
                                label : 'Pembayaran Pinjaman',
                                url: 'pembayaran'
                            }        
                        ]
                    }
                    ,{
                        label : 'Keuangan',
                        children : [
                            {
                                label : 'Jurnal Harian',
                                url : 'jurnal-harian'
                            },{
                                label : 'Tutup Buku',
                                url : 'tutup-buku'
                            },{
                                label : 'Jurnal Penyesuaian',
                                url : 'jurnal-penyesuaian'
                            }
                        ]
                    }
                    
                ]
            },{
                label : 'Laporan',
                children : [
                    {
                        label : 'Koperasi',
                        children : [   
                            {
                                label : 'Lap.Simpanan Anggota',
                                url : 'lap-simpanan'
                            },{
                                label : 'Lap.Pengajuan Pinjaman',
                                url : 'lap-pengajuan-pinjaman'
                            },{
                                label : 'Lap.Pinjaman',
                                url : 'lap-pinjaman'
                            },{
                                label : 'Lap.Pembayaran/Angsuran Pinjaman',
                                url : 'lap-angsuran'
                            },{
                                label : 'Lap. Rekening Koran',
                                url : 'lap-rekeningkoran'
                            }
                        ]
                    },{
                        label : 'Keuangan',
                        children : [
                            {
                                label : 'Jurnal',
                                url : 'lap-jurnal'
                            },{
                                label : 'Buku Besar',
                                url : 'lap-lap-buku-besar'
                            },{
                                label : 'Neraca Saldo',
                                url : 'lap-neraca-saldo'
                            },{
                                label : 'Laba Rugi',
                                url : 'lap-laba-rugi'
                            }
                        ]
                    }
                ]
            }                
        ];
        $scope.my_data = treedata_avm;
        $scope.my_tree = tree = {};

        $http.get( base_url + 'assets/app/lang/'+ setlang).success(function(response) {
            bahasa[0].isi.push(response);
        });

        $scope.KeyUp = function(evt){
            console.log(evt);
        }
        $scope.keyPress = function(keyCode){
           console.log(keyCode); 
        }        
    });

    angular.bootstrap(document.body, ['SukodonoApp']);

});