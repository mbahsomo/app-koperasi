angular.module('SukodonoApp').controllerProvider.register('RekeningkoranController', function($scope, $http, $document, toaster, $filter) {
	$scope.aktif = 't';
    $scope.tgl1 = new Date();
    $scope.tgl2 = new Date();
    $scope.bln = 9;
    $scope.bln_tahun = 2014;
    $scope.tahun = 2014;
    $scope.bulans = [
    	{id:1,name:'Januari'},
    	{id:2,name:'Februari'},
    	{id:3,name:'Maret'},
    	{id:4,name:'April'},
    	{id:5,name:'Mei'},
    	{id:6,name:'Juni'},
    	{id:7,name:'Juli'},
    	{id:8,name:'Agustus'},
    	{id:9,name:'September'},
    	{id:10,name:'Oktober'},
    	{id:11,name:'November'},
    	{id:12,name:'Desember'}
    ];
    
    $scope.cetakPerTanggal = function(){
        if ($scope.tgl1!=='' && $scope.tgl2!==''){
            /*print_preview('/cetakpertanggal/'+$scope.tgl1+'/'+$scope.tgl2,'');*/
            print_preview(base_url + 'assets/app/views/report.html','');
        }else{
            alert('Isi Tanggal Dulu');
        }
    };
    
    $scope.exportPerTanggal = function(){
        var urlprint = "/exportpertanggal/" +$scope.tgl1+'/'+$scope.tgl2;
        var win = window.open(urlprint,'Export','height=255,width=250,toolbar=no,directories=no,status=no,menubar=no,scrollbars=no,resizable=no,top=20,left=20');
        setInterval(
            function(){
                win.close();
                return false;
            },
            5000
        );
    };
    
    $scope.cetakPerBulan = function(){
        if ($scope.bln!=='' && $scope.blntahun!==''){
            /*print_preview('/cetakperbulan/'+$scope.bln+'/'+$scope.bln_tahun,'');*/
            print_preview(base_url + 'assets/app/views/report.html','');
        }else{
            alert('Isi Bulan Dulu');
        }
    };
    
    $scope.exportPerBulan = function(){
        var urlprint = "/exportperbulan/" +$scope.bln+'/'+$scope.bln_tahun;
        var win = window.open(urlprint,'Export','height=255,width=250,toolbar=no,directories=no,status=no,menubar=no,scrollbars=no,resizable=no,top=20,left=20');
        setInterval(
            function(){
                win.close();
                return false;
            },
            7000
        );
    };
    
    $scope.cetakPerTahun = function(){
        if ($scope.tahun!==''){
            /*print_preview('/cetakpertahun/'+$scope.tahun ,'');*/
            print_preview(base_url + 'assets/app/views/report.html','');
        }else{
            alert('Isi tahun Dulu');
        }
    };
    
    $scope.exportPerTahun = function(){
        var urlprint = "/exportpertahun/" +$scope.tahun;
        var win = window.open(urlprint,'Export','height=255,width=250,toolbar=no,directories=no,status=no,menubar=no,scrollbars=no,resizable=no,top=20,left=20');
        setInterval(
            function(){
                win.close();
                return false;
            },
            10000
        );
    };
});