angular.module('SukodonoApp').controllerProvider.register('PembayaranController', function($scope, $http, $document, toaster, $filter) {
    $scope.dataRec = [];
    $scope.dataRecOld = [];
    $scope.dataRecSelect = [];
    $scope.dataRecSelectOld = [];
    $scope.fields = {};
    $scope.add = true;

    $scope.LoadGrid = function() {
        $scope.dataRec = [
            {
                kode : 'A-001',
                name : 'AGUNG NUGROHO',
                depart : 'HRD',
                alamat : 'Surabaya',
                kelamin : 'Laki - laki'
            },{
                kode : 'A-002',
                name : 'BUDI SUSANTO',
                depart : 'HRD',
                alamat : 'Surabaya',
                kelamin : 'Laki - laki'
            }
        ];
    };

    $scope.LoadGrid();

    $scope.gridOptions = {
        data: 'dataRec',
        columnDefs: [
            {field: 'kode', displayName: 'KODE', resizable: true, width: '100px'},
            {field: 'name', displayName: 'Nama', resizable: true, width: '300px'},
            {field: 'depart', displayName: 'Departemen', resizable: true, width: '100px'},
            {field: 'alamat', displayName: 'Alamat', resizable: true, width: '100px'},
            {field: 'kelamin', displayName: 'Kelamin', resizable: true, width: '100px'}        
        ],
        enablePaging: true,
        showFooter: true,
        multiSelect: false,
        enablePinning: true,
        showGroupPanel: true,
        showColumnMenu: true,
        showSelectionCheckbox: true,
        totalServerItems: 'totalServerItems',
        pagingOptions: $scope.pagingOptions,
        filterOptions: $scope.filterOptions,
        selectedItems: $scope.dataRecSelect
    };

    /*Detail grid*/
    $scope.dataRecDetail = [];
    $scope.dataRecSelectDetail= [];
    $scope.$watch('dataRecSelect', function (newVal, oldVal) {
        if (newVal !== oldVal ) {
            $scope.dataRecDetail = $filter('filter')($scope.dataRecOld, {nopeg:newVal[0].kode});
        }
    }, true);

    $scope.LoadGridDetail = function() {
        $scope.dataRecDetail = [
            {
                nopeg : 'A-001',
                tgl : '2014-01-01',
                name : 'Pinjaman',
                nilai : 2000000
            },{
                nopeg : 'A-001',
                tgl : '2014-02-01',
                name : 'Pinjaman',
                nilai : 1000000
            },{
                nopeg : 'A-002',
                tgl : '2014-01-01',
                name : 'Pinjaman',
                nilai : 3000000
            }
        ];
        $scope.dataRecOld = $scope.dataRecDetail;
        $scope.dataRecDetail = $filter('filter')($scope.dataRecOld, {nopeg:'A-001'});
    };
    $scope.LoadGridDetail();

    $scope.gridOptionsDetail = {
        data: 'dataRecDetail',
        columnDefs: [
            {field: 'tgl', displayName: 'Tgl', resizable: true, width: '100px'},
            {field: 'name', displayName: 'Pinjaman', resizable: true},
            {field: 'nilai', displayName: 'Nilai', resizable: true}
        ],
        enablePaging: true,
        //headerRowHeight : 60,
        showFooter: false,
        multiSelect: false,
        enablePinning: true,
        showGroupPanel: false,
        //jqueryUITheme: true,
        showColumnMenu: true,
        showSelectionCheckbox: true,
        totalServerItems: 'totalServerItems',
        selectedItems: $scope.dataRecSelectDetail
                /*rowTemplate :'<div ng-dblclick="onGridDoubleClick(row)" ng-style="{\'cursor\': row.cursor}" ng-repeat="col in visibleColumns()" class="ngCell col{{$index}} {{col.cellClass}}" ng-cell></div>',        */
    };
    
    $scope.ShowAdd = function() {
        if($scope.dataRecSelect[0]===undefined){
            alert('Pilih karyawan dulu');
        }else{
            var rowLen;
            var e = $scope.$on('ngGridEventData', function() {
                $scope.gridOptionsDetail.selectItem(rowLen-1, true);
                e();
            });
            
            rowLen = $scope.dataRecDetail.push({nopeg:$scope.dataRecSelect[0].kode});

            angular.element('#window-modal').modal(
                {
                    top: '150px',
                    backdrop: false,
                    keyboard: false
                }
            );
        }
    };

    $scope.ShowEdit = function() {
        $scope.add = false;
        angular.copy($scope.dataRecSelectDetail, $scope.dataRecSelectOld);
        angular.element('#window-modal').modal(
            {
                top: '150px',
                backdrop: false,
                keyboard: false
            }
        );
    };

    $scope.ShowDelete = function() {
        if (confirm('Anda yakin menghapus data ini? ')) {
            angular.forEach($scope.dataRecSelectDetail, function(rowItem) {
                $scope.dataRecDetail.splice($scope.dataRecDetail.indexOf(rowItem), 1);
            });
        }
    };

    $scope.CancelSave = function(){
        if ($scope.add){
            angular.forEach($scope.dataRecSelectDetail, function(rowItem) {
                $scope.dataRecDetail.splice($scope.dataRecDetail.indexOf(rowItem), 1);
            });
        }else{
            angular.forEach($scope.dataRecSelectDetail, function(rowItem) {
                angular.copy($scope.dataRecSelectOld[0],$scope.dataRecDetail[$scope.dataRecDetail.indexOf(rowItem)]);
            });
        }
        angular.element('#window-modal').modal('hide');
    }

    $scope.Save = function() {
        toaster.pop('success', "Proses Simpan", "Proses Simpan berhasil");
        if ($scope.add){
            var grid = $scope.gridOptionsDetail.ngGrid;
            grid.$viewport.scrollTop(grid.rowMap[ $scope.dataRecDetail.length-1] * grid.config.rowHeight);
            $scope.dataRecOld.push($scope.dataRecDetail[0]);
            console.log($scope.dataRecOld);
        }
        angular.element('#window-modal').modal('hide');
    };

    $scope.Print = function() {
        print_preview('http://lokal.com/latihan/theme-angular/assets/app/awal.js', '');
    };
    
});